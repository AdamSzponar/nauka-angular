import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core'; // import Input

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class FavoriteComponent implements OnInit {

  @Input() isFavorite: boolean;
  @Output() change = new EventEmitter();

  ngOnInit(): void {

  }

  constructor() { }



  icon = 'x';

  onClick() {
    this.change.emit({ newValue: this.isFavorite });

    this.isFavorite = !this.isFavorite;
    if (this.isFavorite) {
      this.icon = 'star';
    } else {
      this.icon = 'x';
    }
  }
}

export interface FavoriteChangeEventArgs{
  isFavorite: boolean
}
