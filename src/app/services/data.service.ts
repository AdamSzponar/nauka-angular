import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { throwError, timer } from 'rxjs';
import { catchError, map, timeInterval, delay, mergeMap } from 'rxjs/operators';
import { AppError } from '../errors/app-error';
import { NotFoundError } from '../errors/not-found-error';
import { BadInput } from '../errors/bad-input';



@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private url: string, private http: HttpClient) {

  }
  getAll() {
    return this.http.get(this.url)
    .pipe(
      delay(1000),
      map(response => response),
      catchError(this.handleError)
    );
  }

  create(resource) {

    // return timer(3000)
    // .pipe(
    //   mergeMap(e => throwError(new AppError()))
    // );

    return this.http.post(this.url, JSON.stringify(resource))
    .pipe(
      catchError(this.handleError)
    );

  }

  update(resource) {
    return this.http.patch(this.url + '/' + resource.id , JSON.stringify({isRead: true}))
    .pipe(
      catchError(this.handleError)
    );
  }

  delete(id) {
    return this.http.delete(`${this.url}/${id}`)
    .pipe(
      catchError(this.handleError)
      );
  }

  private handleError(error: Response) {
    if(error.status === 404) {
      console.log('error sie wywala w serwisie', error);
      return throwError(new NotFoundError(error));
    }

    if(error.status === 400) {
      console.log('error sie wywala w serwisie', error);
      return throwError(new BadInput(error));
    }
    return throwError(new AppError(error));
    console.log('error sie wywala', error);
  }

}
