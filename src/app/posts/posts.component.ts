import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { AppError } from '../errors/app-error';
import { NotFoundError } from '../errors/not-found-error';
import { BadInput } from '../errors/bad-input';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts;

  constructor(private service: PostService) {

   }

  ngOnInit() {
    this.service.getAll()
    .subscribe(
      response => {
      this.posts = response;
      console.log('get all ', response);
    });
  }

  createPost(input) {
    const post: { id?: number, title: string} = { title: input.value };
    console.log(post);
    this.posts.splice(0, 0, post);

    this.service.create(post)
    .subscribe(
      newPost => {
      post['id'] = newPost['id'];

    }, (error: AppError) => {
      this.posts.splice(0,1);
      if (error instanceof BadInput) {
        //  this.form.setErrors(error.json()); // gdy ma sie reactive form zdefiniowane w komponencie
        //  this.form.setErrors(error.originalError); // gdy ma sie reactive form zdefiniowane w komponencie
      } else {
        throw error;
      }

    });
  }

  updatePost(post) {
    this.service.update(post)
    .subscribe(
      response => {
      console.log(response);
    }); // tylko ta czesc ktora chcemy zmienic
    // this.http.put(this.postsUrl, JSON.stringify(post)); //trzeba cały obiekt
  }

  deletePost(post) {
    const indexOfPost = this.posts.indexOf(post);
    this.posts.splice(indexOfPost, 1);

    this.service.delete(post.id)
    .subscribe(
      () => {
    }, (error: AppError) => {
      this.posts.splice(indexOfPost, 0, post);
      if(error instanceof NotFoundError) {
        alert('blad 404!');
      } else {
        throw error;
      }
    });
  }



}
