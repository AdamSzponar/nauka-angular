import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-switchlesson',
  templateUrl: './switchlesson.component.html',
  styleUrls: ['./switchlesson.component.css']
})
export class SwitchlessonComponent implements OnInit {

  viewMode = "map";
  courses = [
    {id: 1, name:"course1"},
    {id: 2, name:"course2"},
    {id: 3, name:"course3"}
  ];

  courses2: any[];

  constructor() { }

  ngOnInit() {
  }

  changeViewMode(viewMode) {
    console.log('click ', viewMode);
    this.viewMode = viewMode;
  }

  add(){
    let maxId = Math.max(...this.courses.map(c => c.id)) + 1;
    this.courses.push({ id: maxId, name: `course${maxId.toString()}`});
    console.log('adding', maxId, this.courses);
  }

  remove(index) {
    console.log('removing index: ', index, this.courses);
    this.courses.splice(index, index);
  }

  loadCourses() {
    this.courses2 = [
      {id: 1, name:"asd"},
      {id: 2, name:"asd123"},
      {id: 3, name:"asdasd22"}
    ];
  }

  trackById(index, course) {
      return course ? course.id : undefined;
  }

}
