import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchlessonComponent } from './switchlesson.component';

describe('SwitchlessonComponent', () => {
  let component: SwitchlessonComponent;
  let fixture: ComponentFixture<SwitchlessonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwitchlessonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchlessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
