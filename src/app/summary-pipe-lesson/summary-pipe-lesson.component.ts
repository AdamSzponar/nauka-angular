import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-summary-pipe-lesson',
  templateUrl: './summary-pipe-lesson.component.html',
  styleUrls: ['./summary-pipe-lesson.component.css']
})
export class SummaryPipeLessonComponent implements OnInit {

  public lorem = 'Nam nulla ex, volutpat ac libero a, rutrum interdum augue. Cras magna ligula, congue ullamcorper neque eget, vehicula sollicitudin nulla. Nulla fringilla, urna ut blandit tincidunt, dui velit porta lectus, ac blandit lectus enim a neque. Morbi vulputate leo a blandit luctus. Nullam fermentum bibendum purus at tempor. Sed varius a leo at rhoncus. Maecenas efficitur eu tortor quis fermentum. Nunc eleifend, dui ut finibus sagittis, arcu enim suscipit metus, condimentum porttitor est neque efficitur arcu. Phasellus non mauris leo. Vivamus sit amet odio dui. Vestibulum aliquam condimentum velit vitae elementum. Nulla euismod quam quis ultricies lobortis. Morbi id varius turpis. Duis quis tempor orci, eu mattis dolor. Fusce sit amet magna pharetra, congue est quis, aliquet lacus.';
  constructor() { }

  ngOnInit() {
  }

}
