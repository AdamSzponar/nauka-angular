import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryPipeLessonComponent } from './summary-pipe-lesson.component';

describe('SummaryPipeLessonComponent', () => {
  let component: SummaryPipeLessonComponent;
  let fixture: ComponentFixture<SummaryPipeLessonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryPipeLessonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryPipeLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
