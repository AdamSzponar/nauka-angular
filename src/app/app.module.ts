import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoursesComponent } from './app.courses.component';
import { CourseComponent } from './course/course.component';
import { CoursesService } from './app.courses.service';
import { AuthorsComponent } from './authors/authors.component';
import { AuthorService } from './author.service';
import { IsActiveLessonComponent } from './is-active-lesson/is-active-lesson.component';
import { SummaryPipeLessonComponent } from './summary-pipe-lesson/summary-pipe-lesson.component';
import { SummaryPipe } from './summary-pipe-lesson/summary-pipe';
import { FavoriteComponent } from './favorite/favorite.component';
import { OcticonDirective } from './shared/octicon-directive';
import { PanelComponent } from './panel/panel.component';
import { IflessonComponent } from './iflesson/iflesson.component';
import { SwitchlessonComponent } from './switchlesson/switchlesson.component';
import bootstrap from "bootstrap";
import { InputFormatDirective } from './input-format.directive';
import { PostsComponent } from './posts/posts.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { PostService } from './services/post.service';
import { AppErrorHandler } from './errors/app-error-handler';


@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    CourseComponent,
    AuthorsComponent,
    IsActiveLessonComponent,
    SummaryPipeLessonComponent,
    SummaryPipe,
    FavoriteComponent,
    OcticonDirective,
    PanelComponent,
    IflessonComponent,
    SwitchlessonComponent,
    InputFormatDirective,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule, //angular
  ],
  providers: [ // do singletonowej dependecy injection
    CoursesService,
    AuthorService,
    HttpClient,
    PostService,
    { provide: ErrorHandler, useClass: AppErrorHandler}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
