import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-is-active-lesson',
  templateUrl: './is-active-lesson.component.html',
  styleUrls: ['./is-active-lesson.component.css']
})
export class IsActiveLessonComponent implements OnInit {

  buttons: Button[] = [{Active: false}, {Active: false}, {Active: false}, {Active: false}, {Active: false}];
  twoWay = 'dane do two way';
  oneWay = 'one way ttttt';

  constructor() { }

  ngOnInit() {
  }

  setActive(index, $event: MouseEvent){
    for(let button of this.buttons) {
      button.Active = false;
    }
    this.buttons[index].Active = true;
    console.log($event);
  }

  sendInput(value:string, $event){
    console.log(value);
    console.log(this.oneWay);
    console.log(this.twoWay);
  }

}

class Button{
  public Active = false;
}
