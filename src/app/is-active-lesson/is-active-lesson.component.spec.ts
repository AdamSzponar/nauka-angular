import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IsActiveLessonComponent } from './is-active-lesson.component';

describe('IsActiveLessonComponent', () => {
  let component: IsActiveLessonComponent;
  let fixture: ComponentFixture<IsActiveLessonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IsActiveLessonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IsActiveLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
