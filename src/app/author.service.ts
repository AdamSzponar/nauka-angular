import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor() { }

  getAuthors() {
    return ['Stephen Kingos', 'Frank Herbertos', 'Tolkienos', 'JRR Martinos'];
  }
}
