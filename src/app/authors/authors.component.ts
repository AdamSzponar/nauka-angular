import { Component, OnInit } from '@angular/core';
import { AuthorService } from '../author.service';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  authors;
  jakisTekst = "jakis tekst hehe";
  colSpan = 2;
  colSpan2 = 3;

  constructor(authorsService: AuthorService) {
    this.authors = authorsService.getAuthors();
   }

   get Authors() {
     return this.authors;
   }

  ngOnInit() {
  }

}
