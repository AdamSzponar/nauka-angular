import { Directive, Input, Output, ElementRef, Renderer2, OnInit } from '@angular/core';

import * as oct from 'octicons';

@Directive({
  selector: '[octicon]',
})

export class OcticonDirective implements OnInit{

  icon: Node;

  @Input('octicon') octicon: string;
  @Input() color: string;
  @Input() width: number;



  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  ngOnInit(): void {
    const el: HTMLElement = this.elementRef.nativeElement;
    el.innerHTML = oct[this.octicon].toSVG();

    this.icon = el.firstChild;
    if(this.color) {
      this.renderer.setStyle(this.icon, 'color', this.color)
    }
    if(this.width) {
      this.renderer.setStyle(this.icon, 'width', this.width);
      this.renderer.setStyle(this.icon, 'height', '100%');
    }
  }


}
