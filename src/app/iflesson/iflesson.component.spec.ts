import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IflessonComponent } from './iflesson.component';

describe('IflessonComponent', () => {
  let component: IflessonComponent;
  let fixture: ComponentFixture<IflessonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IflessonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IflessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
