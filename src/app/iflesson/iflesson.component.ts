import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-iflesson',
  templateUrl: './iflesson.component.html',
  styleUrls: ['./iflesson.component.css']
})
export class IflessonComponent implements OnInit {

  constructor() { }

  courses = ['course1', 'course2', 'course3']

  ngOnInit() {
  }

}
